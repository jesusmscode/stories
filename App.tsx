import React from "react";
import { Provider as PaperProvider } from "react-native-paper";

import theme from "./src/presenation/theme/theme";
import AppNavigator from "./src/presenation/navigation";

const App = () => {
  return (
    <PaperProvider theme={theme}>
      <AppNavigator />
    </PaperProvider>
  );
};

export default App;
