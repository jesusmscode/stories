import { Film } from "./film";

export interface FilmDetail extends Film {
  runtime: number;
  budget: number;
  revenue: number;
  production_companies: { id: number; name: string }[];
  original_language: string;
  genres: { id: number; name: string }[];
}
