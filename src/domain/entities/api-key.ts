export interface APIKey {
  key: string;
  value: string;
}
