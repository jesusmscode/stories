const shuffleArray = <T>(array: T[]): T[] => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

export const getRandomPlots = (plots: string[], num: number): string[] => {
  const shuffled = shuffleArray([...plots]);
  return shuffled.slice(0, num);
};
