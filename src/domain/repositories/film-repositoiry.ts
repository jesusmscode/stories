import { Film } from "../entities/film";
import { FilmDetail } from "../entities/film-details";

export interface FilmRepository {
  getFilms(page: number): Promise<Film[]>;
}

export interface FilmDetailRepository {
  getFilmById(id: number): Promise<FilmDetail>;
}
