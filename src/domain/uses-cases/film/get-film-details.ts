import { FilmDetail } from "../../entities/film-details";
import { FilmDetailRepository } from "../../repositories/film-repositoiry";

export const fetchFilmDetailsUseCase =
  (filmDetailRepository: FilmDetailRepository) =>
  async (id: number): Promise<FilmDetail> => {
    return await filmDetailRepository.getFilmById(id);
  };
