import { Film } from "../../entities/film";
import { FilmRepository } from "../../repositories/film-repositoiry";

export const fetchFilmsUseCase =
  (filmRepository: FilmRepository) =>
  async (page: number): Promise<Film[]> => {
    return await filmRepository.getFilms(page);
  };
