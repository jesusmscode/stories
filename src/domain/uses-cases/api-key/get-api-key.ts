import { APIKeyRepository } from "../../../data/sources/local/api-ey-repository";

export const createGetAPIKey = (repository: APIKeyRepository) => {
  return async (key: string): Promise<string | null> => {
    return await repository.getAPIKey(key);
  };
};
