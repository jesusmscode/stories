import { APIKeyRepository } from "../../../data/sources/local/api-ey-repository";

export const createDeleteAPIKey = (repository: APIKeyRepository) => {
  return async (key: string): Promise<void> => {
    await repository.deleteAPIKey(key);
  };
};
