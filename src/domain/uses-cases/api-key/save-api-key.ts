import { APIKeyRepository } from "../../../data/sources/local/api-ey-repository";

export const createSaveAPIKey = (repository: APIKeyRepository) => {
  return async (key: string, value: string): Promise<void> => {
    await repository.saveAPIKey(key, value);
  };
};
