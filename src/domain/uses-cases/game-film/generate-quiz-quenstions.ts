import { getEmojiSummary } from "../../../data/sources/api/services/get-emoji-sumary";
import { QuizQuestion } from "../../entities/quiz-quenstions";
import { getRandomPlots } from "../../repositories/film-game-repository";

export const generateQuizQuestions = async (
  openaiClient: any,
  moviePlots: string[],
  numQuestions = 5,
  maxOptions = 5,
): Promise<QuizQuestion[]> => {
  const selectedPlots = getRandomPlots(moviePlots, numQuestions);
  const questions = await Promise.all(
    selectedPlots.map(async (plot) => {
      const emojiSummary = await getEmojiSummary(openaiClient, plot);
      if (!emojiSummary) {
        throw new Error("Failed to generate emoji summary");
      }
      const options = [
        ...new Set(
          [...moviePlots]
            .filter((opt) => opt !== plot)
            .slice(0, maxOptions - 1)
            .concat(plot),
        ),
      ];
      return {
        question: emojiSummary,
        options: options.sort(() => Math.random() - 0.5),
        correctAnswer: plot,
      };
    }),
  );
  return questions;
};
