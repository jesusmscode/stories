import { SecureStoreDataSource } from "../sources/local/local-store-secture";

export const createAPIKeyRepository = () => {
  const dataSource = SecureStoreDataSource();

  return {
    saveAPIKey: dataSource.saveAPIKey,
    getAPIKey: dataSource.getAPIKey,
    deleteAPIKey: dataSource.deleteAPIKey,
  };
};
