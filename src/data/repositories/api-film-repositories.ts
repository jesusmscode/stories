import { Film } from "../../domain/entities/film";
import { FilmDetail } from "../../domain/entities/film-details";
import {
  FilmDetailRepository,
  FilmRepository,
} from "../../domain/repositories/film-repositoiry";
import {
  mapApiFilmDetailToFilmDetail,
  mapApiFilmToFilm,
} from "../mappers/film-mappers";
import {
  fetchFilmDetails,
  fetchFilms,
} from "../sources/api/services/film-service";

export const apiFilmRepository: FilmRepository = {
  getFilms: async (page: number): Promise<Film[]> => {
    const apiFilms = await fetchFilms(page);
    return apiFilms.map(mapApiFilmToFilm);
  },
};

export const apiFilmDetailRepository: FilmDetailRepository = {
  getFilmById: async (id: number): Promise<FilmDetail> => {
    const apiFilmDetail = await fetchFilmDetails(id);
    return mapApiFilmDetailToFilmDetail(apiFilmDetail);
  },
};
