export interface APIKeyRepository {
  saveAPIKey(key: string, value: string): Promise<void>;
  getAPIKey(key: string): Promise<string | null>;
  deleteAPIKey(key: string): Promise<void>;
}
