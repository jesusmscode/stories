import * as SecureStore from "expo-secure-store";

export const SecureStoreDataSource = () => {
  const saveAPIKey = async (key: string, value: string): Promise<void> => {
    try {
      await SecureStore.setItemAsync(key, value);
    } catch (error) {
      console.error("Failed to save the API key", error);
    }
  };

  const getAPIKey = async (key: string): Promise<string | null> => {
    try {
      const apiKey = await SecureStore.getItemAsync(key);
      return apiKey;
    } catch (error) {
      console.error("Failed to get the API key", error);
      return null;
    }
  };

  const deleteAPIKey = async (key: string): Promise<void> => {
    try {
      await SecureStore.deleteItemAsync(key);
    } catch (error) {
      console.error("Failed to delete the API key", error);
    }
  };

  return {
    saveAPIKey,
    getAPIKey,
    deleteAPIKey,
  };
};
