export interface ApiResponse<T> {
  results: T;
}

export interface ApiFilm {
  id: number;
  title: string;
  overview: string;
  poster_path: string;
  vote_average: number;
  release_date: string;
}

export interface ApiFilmDetail extends ApiFilm {
  runtime: number;
  budget: number;
  revenue: number;
  production_companies: { id: number; name: string }[];
  original_language: string;
  genres: { id: number; name: string }[];
}
