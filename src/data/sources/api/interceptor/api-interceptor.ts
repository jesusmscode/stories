export const apiCall = async (
  url: string,
  options: RequestInit = {}
): Promise<any> => {
  options.headers = {
    ...options.headers,
    "Content-Type": "application/json",
  };
  const response = await fetch(url, options);
  if (!response.ok) {
    throw new Error("Failed to fetch data from TMDB");
  }
  return response.json();
};
