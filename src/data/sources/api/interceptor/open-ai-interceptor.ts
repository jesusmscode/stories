import axios from "axios";

const OPENAI_BASE_URL = process.env.EXPO_PUBLIC_URL_OPEN_AI;

export const createOpenAIClient = (apiKey: string) => {
  if (!apiKey) {
    throw new Error("OpenAI API key is not defined");
  }

  return axios.create({
    baseURL: OPENAI_BASE_URL,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${apiKey}`,
    },
  });
};
