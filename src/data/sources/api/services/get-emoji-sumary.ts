export const getEmojiSummary = async (
  openaiClient: any,
  plot: string,
): Promise<string | undefined> => {
  try {
    const response = await openaiClient.post("", {
      model: "gpt-3.5-turbo",
      messages: [
        {
          role: "system",
          content:
            "You will be provided with movie titles and your task is to translate them into emojis that best represent it, telling the story of the movie. Don't use any normal text. Do your best with only emojis. Maximum 5 emojis",
        },
        {
          role: "user",
          content: plot,
        },
      ],
      temperature: 0.8,
      max_tokens: 64,
      top_p: 1,
    });

    return response.data.choices[0].message.content.trim();
  } catch (error) {
    console.error("Error fetching emoji summary:", error);
  }
};
