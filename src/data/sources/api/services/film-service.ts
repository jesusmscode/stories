import { apiCall } from "../interceptor/api-interceptor";
import { ApiFilm, ApiFilmDetail, ApiResponse } from "../type-responses/respose";

const TMDB_API_KEY = process.env.EXPO_PUBLIC_API_TMDB_KEY;

export const fetchFilmDetails = async (
  filmId: number,
): Promise<ApiFilmDetail> => {
  return await apiCall(
    `https://api.themoviedb.org/3/movie/${filmId}?api_key=${TMDB_API_KEY}`,
  );
};

export const fetchFilms = async (page: number): Promise<ApiFilm[]> => {
  const response: ApiResponse<ApiFilm[]> = await apiCall(
    `https://api.themoviedb.org/3/movie/top_rated?api_key=${TMDB_API_KEY}&page=${page}`,
  );
  return response.results;
};
