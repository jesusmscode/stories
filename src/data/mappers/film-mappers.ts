import { Film } from "../../domain/entities/film";
import { FilmDetail } from "../../domain/entities/film-details";
import { ApiFilm, ApiFilmDetail } from "../sources/api/type-responses/respose";

export const mapApiFilmToFilm = (apiFilm: ApiFilm): Film => ({
  id: apiFilm.id,
  title: apiFilm.title,
  overview: apiFilm.overview,
  poster_path: apiFilm.poster_path,
  vote_average: apiFilm.vote_average,
  release_date: apiFilm.release_date,
});

export const mapApiFilmDetailToFilmDetail = (
  apiFilmDetail: ApiFilmDetail,
): FilmDetail => ({
  ...mapApiFilmToFilm(apiFilmDetail),
  runtime: apiFilmDetail.runtime,
  budget: apiFilmDetail.budget,
  revenue: apiFilmDetail.revenue,
  production_companies: apiFilmDetail.production_companies,
  original_language: apiFilmDetail.original_language,
  genres: apiFilmDetail.genres,
});
