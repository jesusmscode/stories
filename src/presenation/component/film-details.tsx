import React, { FC } from "react";
import { View, StyleSheet } from "react-native";
import { Subheading } from "react-native-paper";
import ChipList from "./chip-list";
import { FilmDetail } from "../../domain/entities/film-details";

const FilmDetails: FC<{ film: FilmDetail }> = ({ film }) => {
  return (
    <View style={styles.detailsContainer}>
      <Subheading>Release Date: {film.release_date}</Subheading>
      <Subheading>Runtime: {film.runtime} minutes</Subheading>
      <Subheading>Budget: ${film.budget.toLocaleString()}</Subheading>
      <Subheading>Revenue: ${film.revenue.toLocaleString()}</Subheading>
      <Subheading>Production Companies:</Subheading>
      <ChipList items={film.production_companies.map((c) => c.name)} />
      <Subheading>
        Original Language: {film.original_language.toUpperCase()}
      </Subheading>
      <Subheading>Genres:</Subheading>
      <ChipList items={film.genres.map((g) => g.name)} />
    </View>
  );
};

const styles = StyleSheet.create({
  detailsContainer: {
    marginTop: 16,
  },
});

export default FilmDetails;
