import React from "react";
import { TouchableOpacity, Image, StyleSheet } from "react-native";
import { List, useTheme } from "react-native-paper";

interface Film {
  id: number;
  title: string;
  overview: string;
  poster_path: string;
}

const styles = StyleSheet.create({
  itemContainer: {
    paddingHorizontal: 20,
  },
  posterImage: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
});

const FilmListItem: React.FC<{
  film: Film;
  onPress: () => void;
}> = ({ film, onPress }) => {
  const { colors } = useTheme();
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.itemContainer,
        { borderBottomWidth: 1, borderBottomColor: "#ccc" },
      ]}
    >
      <List.Item
        titleStyle={{
          fontSize: 18,
          fontWeight: "bold",
          color: colors.primary,
        }}
        title={film.title}
        description={film.overview}
        left={() => (
          <Image
            source={{
              uri: `https://image.tmdb.org/t/p/w500${film.poster_path}`,
            }}
            style={styles.posterImage}
          />
        )}
      />
    </TouchableOpacity>
  );
};

export default React.memo(FilmListItem);
