import React from "react";
import Message from "./message";

const FeedbackMessage = ({ feedbackMessage }) => (
  <Message type="success" text={feedbackMessage} />
);

export default FeedbackMessage;
