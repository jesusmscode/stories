import React from "react";
import { Text } from "react-native-paper";
import { StyleSheet } from "react-native";
import CustomCard from "./cunstom-card";

interface QuestionCardProps {
  question: string;
}

export const QuestionCard: React.FC<QuestionCardProps> = ({ question }) => (
  <CustomCard style={styles.card}>
    <Text style={styles.emojiText}>{question}</Text>
  </CustomCard>
);

const styles = StyleSheet.create({
  card: {
    marginBottom: 20,
  },
  emojiText: {
    fontSize: 40,
    textAlign: "center",
  },
});
