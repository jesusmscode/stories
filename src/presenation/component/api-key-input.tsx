import React from "react";
import { StyleSheet } from "react-native";
import { TextInput } from "react-native-paper";
import { Controller, Control, FieldErrors } from "react-hook-form";

interface ApiKeyInputProps {
  control: Control<any>;
  errors: FieldErrors<any>;
  isApiKeyVisible: boolean;
}

const ApiKeyInput: React.FC<ApiKeyInputProps> = ({
  control,
  errors,
  isApiKeyVisible,
}) => (
  <Controller
    name="apiKey"
    control={control}
    render={({ field: { onChange, onBlur, value } }) => (
      <TextInput
        label="OpenAI API Key"
        mode="flat"
        secureTextEntry={!isApiKeyVisible}
        onBlur={onBlur}
        onChangeText={onChange}
        value={value}
        error={!!errors.apiKey}
        style={styles.input}
      />
    )}
  />
);

const styles = StyleSheet.create({
  input: {
    marginBottom: 20,
    fontSize: 16,
  },
});

export default ApiKeyInput;
