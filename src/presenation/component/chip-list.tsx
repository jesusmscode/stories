import React, { FC } from "react";
import { View, StyleSheet } from "react-native";
import { Chip, useTheme } from "react-native-paper";

const ChipList: FC<{ items: string[] }> = ({ items }) => {
  const { colors } = useTheme();
  return (
    <View style={styles.chipsContainer}>
      {items.map((item, index) => (
        <Chip
          key={index}
          textStyle={{ color: colors.surface }}
          style={[styles.chip, { backgroundColor: colors.primary }]}
        >
          {item}
        </Chip>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  chipsContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 8,
  },
  chip: {
    marginRight: 8,
    marginBottom: 8,
  },
});

export default ChipList;
