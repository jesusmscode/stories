import React from "react";
import Message from "./message";

const ApiKeyError = ({ error }) => (
  <Message type="error" text={error ? error.message : ""} />
);

export default ApiKeyError;
