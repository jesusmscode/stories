import React from "react";
import { View, StyleSheet } from "react-native";
import { Button } from "react-native-paper";

interface ActionButtonsProps {
  handleSubmit: (callback: (data: { apiKey: string }) => void) => () => void;
  onSubmit: (data: { apiKey: string }) => Promise<void>;
  onDelete: () => void;
  toggleApiKeyVisibility: () => void;
  isApiKeySaved: boolean;
  isApiKeyVisible: boolean;
}

const ActionButtons: React.FC<ActionButtonsProps> = ({
  handleSubmit,
  onSubmit,
  onDelete,
  toggleApiKeyVisibility,
  isApiKeySaved,
  isApiKeyVisible,
}) => (
  <View style={styles.buttonContainer}>
    <Button
      icon="content-save"
      mode="contained"
      onPress={handleSubmit(onSubmit)}
      style={styles.button}
    >
      Save
    </Button>
    <Button
      icon="delete"
      mode="outlined"
      onPress={onDelete}
      style={styles.button}
      disabled={!isApiKeySaved}
    >
      Delete
    </Button>
    <Button
      icon={isApiKeyVisible ? "eye-off" : "eye"}
      mode="outlined"
      onPress={toggleApiKeyVisibility}
      style={styles.button}
    >
      {isApiKeyVisible ? "Hide" : "Show"}
    </Button>
  </View>
);

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: "row",
    marginTop: 10,
  },
  button: {
    marginHorizontal: 5,
  },
});

export default ActionButtons;
