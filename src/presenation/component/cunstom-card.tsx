import React from "react";
import { Card, CardProps } from "react-native-paper";

interface CustomCardProps extends CardProps {
  style: any;
}

const CustomCard: React.FC<CustomCardProps> = (props: CustomCardProps) => {
  return (
    <Card>
      <Card.Content>{props.children}</Card.Content>
    </Card>
  );
};

export default CustomCard;
