import React from "react";
import { StyleSheet, Text } from "react-native";

const Message = ({ type, text }) => {
  if (!text) return null;

  return <Text style={[styles.messageText, styles[type]]}>{text}</Text>;
};

const styles = StyleSheet.create({
  messageText: {
    marginTop: 16,
    textAlign: "center",
    fontSize: 16,
  },
  success: {
    color: "green",
  },
  error: {
    color: "red",
  },
});

export default Message;
