import React from "react";
import { ActivityIndicator, ActivityIndicatorProps } from "react-native-paper";
import { View, StyleSheet } from "react-native";

interface CustomActivityIndicatorProps extends ActivityIndicatorProps {
  style?: object;
}

const CustomActivityIndicator: React.FC<CustomActivityIndicatorProps> = (
  props,
) => {
  return (
    <View style={[styles.container, props.style]}>
      <ActivityIndicator {...props} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default CustomActivityIndicator;
