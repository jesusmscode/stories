import React from "react";
import { Button, useTheme } from "react-native-paper";
import { StyleSheet, Text } from "react-native";

interface OptionButtonProps {
  option: string;
  onPress: () => void;
  disabled: boolean;
  selected: boolean;
  correct: boolean | null;
}

export const OptionButton: React.FC<OptionButtonProps> = ({
  option,
  onPress,
  disabled,
  selected,
  correct,
}) => {
  const { colors } = useTheme();
  return (
    <Button
      mode="contained"
      style={[
        styles.optionButton,
        selected && {
          backgroundColor: correct ? colors.correcto : colors.error,
        },
      ]}
      onPress={onPress}
      disabled={disabled}
    >
      <Text style={{ color: colors.surface }}>{option}</Text>
    </Button>
  );
};

const styles = StyleSheet.create({
  optionButton: {
    marginBottom: 10,
  },
});
