import React, { FC } from "react";
import { View, StyleSheet, Image } from "react-native";
import {
  Card,
  Title,
  Paragraph,
  ProgressBar,
  useTheme,
} from "react-native-paper";
import FilmDetails from "./film-details";
import { FilmDetail } from "../../domain/entities/film-details";

const FilmCard: FC<{ film: FilmDetail }> = ({ film }) => {
  const { colors } = useTheme();
  return (
    <Card>
      <View style={styles.imageAndRatingContainer}>
        <Image
          source={{
            uri: `https://image.tmdb.org/t/p/w500${film.poster_path}`,
          }}
          style={styles.image}
        />
      </View>
      <Card.Content>
        <Paragraph
          style={{
            fontWeight: "bold",
            marginVertical: 10,
            flexDirection: "row",
            alignSelf: "center",
            fontSize: 20,
            color: "#3498db",
          }}
        >
          {film.vote_average.toFixed(1)}
        </Paragraph>
        <ProgressBar
          progress={film.vote_average / 10}
          color={"#3498db"}
          style={styles.progressBar}
        />
        <Title style={[styles.title, { color: colors.primary }]}>
          {film.title}
        </Title>
        <Paragraph>{film.overview}</Paragraph>
        <FilmDetails film={film} />
      </Card.Content>
    </Card>
  );
};

const styles = StyleSheet.create({
  imageAndRatingContainer: {
    width: "100%",
    marginVertical: 16,
  },
  image: {
    width: "100%",
    height: 200,
    resizeMode: "contain",
  },
  title: {
    fontWeight: "bold",
    fontSize: 24,
    marginVertical: 16,
  },
  progressBar: {
    marginVertical: 8,
  },
});

export default FilmCard;
