import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { FilmsScreen } from "../screens/film-screen";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import GameScreen from "../screens/game-screen";
import SettingsScreen from "../screens/setting-screen";
import FilmDetailScreen from "../screens/film-details";

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const FilmsStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="Films" component={FilmsScreen} />
    <Stack.Screen name="FilmDetail" component={FilmDetailScreen} />
  </Stack.Navigator>
);

const AppNavigator = () => (
  <NavigationContainer>
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: "#3498db",
      }}
    >
      <Tab.Screen
        name="FilmsTab"
        component={FilmsStack}
        options={{
          tabBarLabel: "Films",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="movie" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Game"
        component={GameScreen}
        options={{
          tabBarLabel: "Game",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="gamepad-variant"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          headerShown: false,
          tabBarLabel: "Settings",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="cog" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  </NavigationContainer>
);

export default AppNavigator;
