import React from "react";
import { View, StyleSheet } from "react-native";
import useApiKey from "../hooks/use-api-key";
import CustomCard from "../component/cunstom-card";
import ApiKeyInput from "../component/api-key-input";
import Message from "../component/message";
import ActionButtons from "../component/action-button";
import { useTheme } from "react-native-paper";

const SettingsScreen = () => {
  const {
    control,
    handleSubmit,
    errors,
    message,
    isApiKeyVisible,
    isApiKeySaved,
    onSubmit,
    onDelete,
    toggleApiKeyVisibility,
  } = useApiKey();
  const { colors } = useTheme();
  return (
    <View style={[styles.container, { backgroundColor: colors.surface }]}>
      <CustomCard style={styles.card}>
        <ApiKeyInput
          control={control}
          errors={errors}
          isApiKeyVisible={isApiKeyVisible}
        />
        <Message
          type="error"
          text={errors.apiKey ? errors.apiKey.message : ""}
        />
        <ActionButtons
          handleSubmit={handleSubmit}
          onSubmit={(data: { apiKey: string }) => onSubmit(data)}
          onDelete={onDelete}
          toggleApiKeyVisibility={toggleApiKeyVisibility}
          isApiKeySaved={isApiKeySaved}
          isApiKeyVisible={isApiKeyVisible}
        />
        <Message type={message.type} text={message.text} />
      </CustomCard>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 20,
  },
  card: {
    borderRadius: 10,
    elevation: 3,
  },
});

export default SettingsScreen;
