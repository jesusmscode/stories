import React, { FC } from "react";
import { View, ScrollView, StyleSheet, Text } from "react-native";
import { useRoute, RouteProp } from "@react-navigation/native";
import CustomActivityIndicator from "../component/custom-activity-indicator";

import FilmCard from "../component/film-card";
import { useFilmDetails } from "../hooks/use-fim-details";

type RootStackParamList = {
  FilmDetail: { filmId: number };
};

type FilmDetailScreenRouteProp = RouteProp<RootStackParamList, "FilmDetail">;

const FilmDetailScreen: FC = () => {
  const route = useRoute<FilmDetailScreenRouteProp>();
  const { filmId } = route.params;
  const { film, loading, error } = useFilmDetails(filmId);

  if (loading) {
    return (
      <CustomActivityIndicator
        animating={true}
        size="large"
        style={styles.loader}
      />
    );
  }

  if (error) {
    return (
      <View style={styles.errorContainer}>
        <Text style={styles.errorText}>{error}</Text>
      </View>
    );
  }

  return (
    <ScrollView style={styles.container}>
      {film && <FilmCard film={film} />}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  errorContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  errorText: {
    color: "red",
    fontSize: 18,
  },
});

export default FilmDetailScreen;
