import React from "react";
import { FlatList } from "react-native";
import { useNavigation, NavigationProp } from "@react-navigation/native";
import CustomActivityIndicator from "../component/custom-activity-indicator";
import FilmListItem from "../component/film-list-item";
import { useFilms } from "../hooks/use-film";
import { Film } from "../../domain/entities/film";

type RootStackParamList = {
  FilmDetail: { filmId: number };
};

export const FilmsScreen: React.FC = () => {
  const { films, loading, loadingMore, handleLoadMore } = useFilms();
  const navigation = useNavigation<NavigationProp<RootStackParamList>>();

  const renderItem = ({ item }: { item: Film }) => (
    <FilmListItem
      film={item}
      onPress={() => navigation.navigate("FilmDetail", { filmId: item.id })}
    />
  );

  if (loading && !loadingMore) {
    return <CustomActivityIndicator />;
  }

  return (
    <FlatList
      data={films as unknown as ArrayLike<Film>}
      keyExtractor={(item, index) => index.toString()}
      renderItem={renderItem}
      onEndReached={handleLoadMore}
      onEndReachedThreshold={0.5}
      ListFooterComponent={loadingMore ? <CustomActivityIndicator /> : null}
    />
  );
};

export default FilmsScreen;
