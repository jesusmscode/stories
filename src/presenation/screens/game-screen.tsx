import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { Text } from "react-native-paper";
import { useFocusEffect } from "@react-navigation/native";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { useQuiz } from "../hooks/use-quiz";
import { QuestionCard } from "../component/question-card";
import { OptionButton } from "../component/optionButton";
import CustomActivityIndicator from "../component/custom-activity-indicator";

interface GameScreenProps {
  navigation: any;
}

const GameScreen: React.FC<GameScreenProps> = ({ navigation }) => {
  const {
    questions,
    currentQuestionIndex,
    selectedOption,
    isCorrect,
    score,
    loading,
    error,
    checkApiKey,
    handleOptionPress,
  } = useQuiz();

  useFocusEffect(
    React.useCallback(() => {
      checkApiKey(navigation);
    }, [checkApiKey, navigation]),
  );

  if (loading) {
    return (
      <CustomActivityIndicator
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        animating={true}
        size="large"
      />
    );
  }

  if (error) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>{error}</Text>
      </View>
    );
  }

  const currentQuestion = questions[currentQuestionIndex];

  return (
    <GestureHandlerRootView style={styles.container}>
      <ScrollView style={{ alignSelf: "center", width: "100%" }}>
        <View style={styles.containerTittle}>
          <Text style={styles.Tittle}>What is the film?</Text>
          <Text style={styles.scoreText}>
            {score}/{questions.length}
          </Text>
        </View>
        <Text style={styles.questionNumber}>
          Question: {currentQuestionIndex + 1}
        </Text>
        <QuestionCard question={currentQuestion.question} />
        <View style={styles.optionsContainer}>
          {currentQuestion.options.map((option, index) => (
            <OptionButton
              key={index}
              option={option}
              onPress={() => handleOptionPress(option)}
              disabled={selectedOption !== null}
              selected={selectedOption === option}
              correct={isCorrect}
            />
          ))}
        </View>
      </ScrollView>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "#e6e6e6",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 50,
  },
  containerTittle: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  card: {
    marginBottom: 20,
  },
  scoreText: {
    marginVertical: 20,
    textAlign: "center",
    fontSize: 38,
  },
  Tittle: {
    marginVertical: 20,
    textAlign: "center",
    fontSize: 30,
  },
  questionNumber: {
    textAlign: "center",
    fontSize: 16,
    marginVertical: 10,
  },
  optionsContainer: {
    marginVertical: 20,
    flexDirection: "column",
    justifyContent: "space-around",
    gap: 10,
  },
  optionButton: {
    marginBottom: 10,
  },
});

export default GameScreen;
