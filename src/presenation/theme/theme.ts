import { DefaultTheme } from "react-native-paper";

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: "#3498db",
    accent: "#fabada",
    background: "#ffffff",
    surface: "#ffffff",
    text: "#000000",
    error: "#f13a59",
    chipBackground: "red",
    chipText: "#fabada",
    correcto: "#4FD900",

    incorrect: "#EE470D",
  },
};

export default theme;
