import { useState, useEffect } from "react";
import { fetchFilmDetails } from "../../data/sources/api/services/film-service";
import { FilmDetail } from "../../domain/entities/film-details";

export const useFilmDetails = (filmId: number) => {
  const [film, setFilm] = useState<FilmDetail | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadFilmDetails = async () => {
      try {
        const data = await fetchFilmDetails(filmId);
        setFilm(data);
      } catch (error) {
        console.error("Failed to fetch film details:", error);
        setError("Failed to fetch film details. Please try again later.");
      } finally {
        setLoading(false);
      }
    };

    loadFilmDetails();
  }, [filmId]);

  return { film, loading, error };
};
