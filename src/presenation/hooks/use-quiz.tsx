import { useState, useCallback, useEffect } from "react";
import { Alert } from "react-native";
import { generateQuizQuestions } from "../../domain/uses-cases/game-film/generate-quiz-quenstions";
import { createOpenAIClient } from "../../data/sources/api/interceptor/open-ai-interceptor";
import { createGetAPIKey } from "../../domain/uses-cases/api-key/get-api-key";
import { createAPIKeyRepository } from "../../data/repositories/crud-api-key-open-ai-repositories";
import { moviesNames } from "../../data/films-names";
export interface Question {
  question: string;
  options: string[];
  correctAnswer: string;
}

export const useQuiz = () => {
  const [questions, setQuestions] = useState<Question[]>([]);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedOption, setSelectedOption] = useState<string | null>(null);
  const [isCorrect, setIsCorrect] = useState<boolean | null>(null);
  const [score, setScore] = useState(0);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  const apiKeyRepository = createAPIKeyRepository();
  const getAPIKey = createGetAPIKey(apiKeyRepository);

  const fetchQuestions = useCallback(async (apiKey: string) => {
    const openaiClient = createOpenAIClient(apiKey);
    try {
      const quizQuestions = await generateQuizQuestions(
        openaiClient,
        moviesNames,
        10,
        6
      );
      setQuestions(quizQuestions);
      setLoading(false);
    } catch (error) {
      setError("Failed to load quiz questions.");
      console.error("Error fetching quiz questions:", error);
      setLoading(false);
    }
  }, []);

  const checkApiKey = useCallback(
    async (navigation: any) => {
      const apiKey = await getAPIKey("openai_api_key");
      if (!apiKey) {
        Alert.alert(
          "API Key Missing",
          "You need to provide an OpenAI API Key.",
          [
            {
              text: "Go to Settings",
              onPress: () => navigation.navigate("Settings"),
            },
          ]
        );
      } else {
        fetchQuestions(apiKey);
      }
    },
    [fetchQuestions]
  );

  const handleOptionPress = (option: string) => {
    setSelectedOption(option);
    const correct = option === questions[currentQuestionIndex].correctAnswer;
    setIsCorrect(correct);
    if (correct) {
      setScore(score + 1);
    }

    setTimeout(async () => {
      setSelectedOption(null);
      setIsCorrect(null);
      if (currentQuestionIndex < questions.length - 1) {
        setCurrentQuestionIndex(currentQuestionIndex + 1);
      } else {
        Alert.alert(`Quiz finished! Your score: ${score}/${questions.length}`);
        const apiKey = await getAPIKey("openai_api_key");
        if (apiKey) {
          fetchQuestions(apiKey);
        }
        setCurrentQuestionIndex(0);
        setScore(0);
      }
    }, 1000);
  };

  useEffect(() => {
    const loadInitialQuestions = async () => {
      const apiKey = await getAPIKey("openai_api_key");
      if (apiKey) {
        fetchQuestions(apiKey);
      }
    };
    loadInitialQuestions();
  }, [fetchQuestions]);

  return {
    questions,
    currentQuestionIndex,
    selectedOption,
    isCorrect,
    score,
    loading,
    error,
    checkApiKey,
    handleOptionPress,
  };
};
