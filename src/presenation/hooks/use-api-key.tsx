import { useState, useEffect, useCallback } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useFocusEffect } from "@react-navigation/native";
import { createAPIKeyRepository } from "../../data/repositories/crud-api-key-open-ai-repositories";
import { createSaveAPIKey } from "../../domain/uses-cases/api-key/save-api-key";
import { createGetAPIKey } from "../../domain/uses-cases/api-key/get-api-key";
import { createDeleteAPIKey } from "../../domain/uses-cases/api-key/delete-api-key";

const apiKeySchema = z.object({
  apiKey: z
    .string()
    .regex(/^sk-[a-zA-Z0-9-]{1,61}$/, "Invalid OpenAI API key format"),
});

const useApiKey = () => {
  const [message, setMessage] = useState({ type: "", text: "" });
  const [isApiKeyVisible, setIsApiKeyVisible] = useState(false);
  const [isApiKeySaved, setIsApiKeySaved] = useState(false);

  const apiKeyRepository = createAPIKeyRepository();
  const saveAPIKey = createSaveAPIKey(apiKeyRepository);
  const getAPIKey = createGetAPIKey(apiKeyRepository);
  const deleteAPIKey = createDeleteAPIKey(apiKeyRepository);

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: zodResolver(apiKeySchema),
    defaultValues: {
      apiKey: "",
    },
  });

  useFocusEffect(
    useCallback(() => {
      const loadApiKey = async () => {
        const apiKey = await getAPIKey("openai_api_key");
        if (apiKey) {
          reset({ apiKey });
          setIsApiKeySaved(true);
        } else {
          setIsApiKeySaved(false);
        }
      };
      loadApiKey();
    }, [reset])
  );

  useEffect(() => {
    if (message.text) {
      const timer = setTimeout(() => {
        setMessage({ type: "", text: "" });
      }, 2000);
      return () => clearTimeout(timer);
    }
  }, [message]);

  const onSubmit = async (data: { apiKey: string }) => {
    await saveAPIKey("openai_api_key", data.apiKey);
    setMessage({ type: "success", text: "API Key saved successfully!" });
    setIsApiKeySaved(true);
  };

  const onDelete = async () => {
    await deleteAPIKey("openai_api_key");
    reset({ apiKey: "" });
    setMessage({ type: "success", text: "API Key deleted successfully!" });
    setIsApiKeySaved(false);
  };

  const toggleApiKeyVisibility = () => {
    setIsApiKeyVisible(!isApiKeyVisible);
  };

  return {
    control,
    handleSubmit,
    errors,
    message,
    isApiKeyVisible,
    isApiKeySaved,
    onSubmit,
    onDelete,
    toggleApiKeyVisibility,
  };
};

export default useApiKey;
