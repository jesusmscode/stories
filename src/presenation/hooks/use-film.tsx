import { useState, useEffect, useCallback } from "react";
import { fetchFilms } from "../../data/sources/api/services/film-service";

interface Film {
  id: number;
  title: string;
  overview: string;
  poster_path: string;
}

export const useFilms = () => {
  const [films, setFilms] = useState<Film[]>([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [loadingMore, setLoadingMore] = useState(false);

  useEffect(() => {
    const loadFilms = async () => {
      const newFilms = await fetchFilms(page);
      setFilms((prevFilms) => [...prevFilms, ...newFilms]);
      setLoading(false);
      setLoadingMore(false);
    };

    loadFilms();
  }, [page]);

  const handleLoadMore = useCallback(() => {
    if (!loadingMore) {
      setLoadingMore(true);
      setPage((prevPage) => prevPage + 1);
    }
  }, [loadingMore]);

  return { films, loading, loadingMore, handleLoadMore };
};
