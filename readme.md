# stories

## Requisitos Previos

Antes de comenzar, asegúrate de tener instalado lo siguiente:

1. **Node.js** (versión 14 o superior)
2. **npm**

tienes las recomendaciones para prepara tu entorno para trabajar con expo, y el uso de emuladores y simuladores.
https://docs.expo.dev/get-started/set-up-your-environment/

## Instalación

1. Clona el repositorio del proyecto:

   ```
   git clone https://gitlab.com/jesusmscode/stories.git
         cd stories
   ```

2. Instala las dependencias del proyecto:
   ```
   npm install
   ```

## Configuración

1. Crea un archivo `.env` en la raíz del proyecto y añade las credenciales necesarias para las APIs que utilice la aplicación. Por ejemplo:

   ```env
   API_OPEN_AI_KEY=sk-proj-UaWPlkJheaDxKVIPffJETdsfsdfsfsdfsdfsdcsd
   EXPO_PUBLIC_API_TMDB_KEY=95fadffe801533303dfdsfsdfsefewfew
   EXPO_PUBLIC_URL_OPEN_AI="https://api.openai.com/v1/chat/completions"
   ```

las credenciales son de la API de TMDB y una de la API open AI
https://developer.themoviedb.org/reference/intro/getting-started
https://platform.openai.com/docs/quickstart

## Scripts Disponibles

En el archivo `package.json` se encuentran los siguientes scripts que puedes utilizar para ejecutar diferentes comandos:

- `start`: Inicia la aplicación en el modo de desarrollo.
  ```bash
  npm start
  ```
- `android`: Inicia la aplicación en un dispositivo o emulador Android.
  ```bash
  npm run android
  ```
- `ios`: Inicia la aplicación en un dispositivo o emulador iOS.
  ```bash
  npm run ios
  ```
- `web`: Inicia la aplicación en un navegador web.
  ```bash
  npm run web
  ```
- `lint`: Ejecuta ESLint para revisar el código.
  ```bash
  npm run lint
  ```
- `lint:fix`: Ejecuta ESLint y arregla automáticamente los problemas detectados.
  ```bash
  npm run lint:fix
  ```

## Ejecución

1. Para iniciar la aplicación, simplemente ejecuta:

   ```bash
   npm start
   ```

   Esto abrirá Expo DevTools en tu navegador.

2. Para correr la aplicación en Android, iOS o Web, puedes usar los scripts mencionados anteriormente:
   ```bash
   npm run android
   npm run ios
   npm run web
   ```

Expo se encargará de iniciar el emulador correspondiente o abrirá la aplicación en el navegador.
